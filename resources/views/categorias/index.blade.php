<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista de categorias</title>
</head>
<body>
    <h1>Lista Categorias</h1>
    <table class="table table-hover">
       
            <tr>
                <th>Nombre Categoria</th>
                <th>Actualizar</th>
            </tr>
        
            @foreach ($categorias as $c)
            <tr>
                <td>
                    {{$c->name}}
                </td>
                <td><a href=" {{    url('categorias/edit/'.$c->category_id)    }} ">Actualizar</a></td>
            </tr>
            @endforeach
        
    </table>
    {{$categorias->links()}}
</body>
</html>