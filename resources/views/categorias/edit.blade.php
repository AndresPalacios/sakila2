<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css">
    <title>Modificar Categoria</title>
</head>
<body>

  @if(session("mensaje"))
  <p class="alert-success"> {{  session("mensaje")  }} </p>
  @endif
<form method="post" action='{{url("categorias/update/$categorias->category_id") }}' class="form-horizontal">
    @csrf     
    <fieldset>
        
        <!-- Form Name -->
        <legend>Modificar Categoría</legend>
        
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="textinput">Nombre Categoria</label>  
          <div class="col-md-4">
          <input id="categoria" value="{{$categorias->name}}" name="categoria" type="text" placeholder="" class="form-control input-md">
            <strong class="text-danger">{{  $errors->first("categoria")  }}</strong>
          </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="singlebutton"></label>
          <div class="col-md-4">
            <button id="singlebutton" name="singlebutton" class="btn btn-primary">Enivar</button>
          </div>
        </div>
        
        </fieldset>
        </form>
</body>
</html>