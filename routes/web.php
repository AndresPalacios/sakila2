<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
 return view('welcome');
});


    //mi primera ruta el laravel
    Route::get("categorias" , "CategoriaController@index");
    //Ruta mostrar formulario para crear categoria
    Route::get("categorias/create","CategoriaController@create");
    //Ruta para mostrar formulario de actualizar
    Route::get("categorias/edit/{category_id}","CategoriaController@edit");
    //Ruta Para  Actualizar
    Route::post("categorias/update/{category_id}","CategoriaController@update");
    //Ruta para guardar la nueva categoria en BD
    Route::post("categorias/store","CategoriaController@store");